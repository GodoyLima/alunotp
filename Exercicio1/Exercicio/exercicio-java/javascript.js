//exemplo de função seta
"use strict";
//ES5
function somaV1(a, b){
    return(a+b);
}
function exibir(){
    console.log("Chamando a função");
}
function teste(){
    return (10+4+18);
}
//ou
let somaV2 = function(a, b){
    return (a+b);
}

//ES6
let somaV3 = (a, b) => (a+b);
//let somaV3 = (a, b) => { return (a+b);}